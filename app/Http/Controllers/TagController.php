<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    function getTag(Request $request){
        $tag = Tag::get();
        return response()->json($tag, 200);
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }
    
    function createTag(Request $request){
    $data = $request->json()->all();
        $tag = Tag::create([
            'nombre' => $data['nombre'],
        ]);
        return response()->json($tag, 201);
    }

    function deleteTagComplaint(Request $request, $id){
        Tag::destroy($id);
        return response()->json(['Succesfull' => 'Deleted'], 200);
    }
}