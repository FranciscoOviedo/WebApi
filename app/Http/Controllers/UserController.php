<?php

namespace App\Http\Controllers;

use App\User;
use App\Complaint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function createUser(Request $request){
        //if($request->isJson()){
            $data = $request->json()->all();
            $user = User::create([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'anonimo' => $data['anonimo'],
                'password' => Hash::make($data['password']),
                'api_token' => str_random(60)
            ]);
            return response()->json($user, 201);
        //}
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }

    function userComplaints(Request $request, $id){
        //if($request->isJson()){
            //$user = User::find(1)->complaints();
            //$user = User::with('complaints')->get();


            $user = User::with('complaint')->find($id);
            return response()->json($user, 200);


        //}
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }

    function getToken(Request $request){
        if($request->isJson()){
            try{
                $data = $request->json()->all();
                $user = User::where('email', $data['email'])->first();
                if($user && Hash::check($data['password'], $user->password)){
                    return response()->json($user, 200);
                } else {
                    return response()->json(['id' => 0], 200);
                }
            } catch (ModelNotFoundException $e){
                return response()->json(['error' => 'No Content'], 406);
            }
        }
        return response()->json(['error' => 'Unauthorized'], 401, []);
    }
}
