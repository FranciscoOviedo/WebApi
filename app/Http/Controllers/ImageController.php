<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    function getImages(Request $request){
        $image = Image::get();
        return response()->json($image, 200);
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }
    
    function createImage(Request $request){
    $data = $request->json()->all();
        $image = Image::create([
            'image' => $data['image'],
            'id_complaint' => $data['id_complaint']
        ]);
        return response()->json($image, 201);
    }

    function deleteImage(Request $request, $id){
        Image::destroy($id);
        return response()->json(['Succesfull' => 'Deleted'], 200);
    }
}
