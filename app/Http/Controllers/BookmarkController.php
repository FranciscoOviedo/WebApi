<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Bookmark;
use Illuminate\Http\Request;

class BookmarkController extends Controller
{
    function getBookmarks(Request $request){
        $bookmark = Bookmark::get();
        return response()->json($bookmark, 200);
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }
    
    function createBookmark(Request $request){
    $data = $request->json()->all();
        $bookmark = Bookmark::create([
            'id_user' => $data['id_user'],
            'id_complaint' => $data['id_complaint']
        ]);
        return response()->json($bookmark, 201);
    }

    function deleteBookmark(Request $request, $id){
        Bookmark::destroy($id);
        return response()->json(['Succesfull' => 'Deleted'], 200);
    }
}
