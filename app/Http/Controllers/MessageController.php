<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    function getMessages(Request $request){
        $message = Message::get();
        return response()->json($message, 200);
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }
    
    function createMessage(Request $request){
    $data = $request->json()->all();
        $message = Message::create([
            'id_user' => $data['id_user'],
            'id_complaint' => $data['id_complaint'],
            'comment' => $data['comment']
        ]);
        return response()->json($message, 201);
    }

    function deleteMessage(Request $request, $id){
        Message::destroy($id);
        return response()->json(['Succesfull' => 'Deleted'], 200);
    }
}