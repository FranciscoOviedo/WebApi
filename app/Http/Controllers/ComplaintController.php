<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Complaint;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    function getComplaints(Request $request){
        $complaints = Complaint::with('user')->orderBy('created_at', 'desc')->get();
        return response()->json($complaints, 200);
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }

    function getComplaintDetails(Request $request){
        $complaints = Complaint::info()->orderBy('created_at', 'desc')->get();
        return response()->json($complaints, 200);
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }

    function createComplaint(Request $request){
        //if($request->isJson()){
            $data = $request->json()->all();
            $complaint = Complaint::create([
                'severity' => $data['severity'],
                'title' => $data['title'],
                'description' => $data['description'],
                'place' => $data['place'],
                'ranking_plus' => 0,
                'ranking_minus' => 0,
                'id_user' => $data['id_user']
            ]);
            return response()->json($complaint, 201);
        //}
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }
    
    function deleteComplaint(Request $request, $id){
        Complaint::destroy($id);
    }
}
