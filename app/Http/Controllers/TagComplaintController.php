<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\TagComplaint;
use Illuminate\Http\Request;

class TagComplaintController extends Controller
{
    function getTagsComplaints(Request $request){
        $tagComplaint = TagComplaint::get();
        return response()->json($tagComplaint, 200);
        //return response()->json(['error' => 'Unauthorized'], 401, []);
    }
    
    function createTagComplaint(Request $request){
    $data = $request->json()->all();
        $tagComplaint = TagComplaint::create([
            'id_complaint' => $data['id_complaint'],
            'id_tag' => $data['id_tag']
        ]);
        return response()->json($tagComplaint, 201);
    }

    function deleteTagComplaint(Request $request, $id){
        TagComplaint::destroy($id);
        return response()->json(['Succesfull' => 'Deleted'], 200);
    }
}