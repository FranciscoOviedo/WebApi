<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class TagComplaint extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public function tagcomplaint(){
        return $this->belongsTo(Tag::class,'id_tag');
    }

    public function complaint(){
        return $this->belongsTo(Complaint::class,'id_complaint');
    }

    protected $fillable = [
        'id_complaint', 'id_tag'
    ];

    protected $hidden = [
        
    ];
}
