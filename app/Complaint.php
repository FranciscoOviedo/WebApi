<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Complaint extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public function scopeInfo($query){
        return $query->with('user', 'image','bookmark', 'comment');
    }

    public function user(){
        return $this->belongsTo(User::class,'id_user');
    }

    public function image(){
        return $this->hasMany(Image::class,'id_complaint');
    }

    public function bookmark(){
        return $this->hasMany(Bookmark::class,'id_complaint');
    }

    public function comment(){
        return $this->hasMany(Comment::class,'id_complaint');
    }

    protected $fillable = [
        'severity', 'title', 'description', 'place', 'ranking_plus', 'ranking_minus', 'id_user',
    ];

    protected $hidden = [
    ];
}
