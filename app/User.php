<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public function complaint(){
        return $this->hasMany(Complaint::class,'id_user');
    }

    public function bookmark(){
        return $this->hasMany(Bookmark::class,'id_user');
    }

    public function comment(){
        return $this->hasMany(Comment::class,'id_user');
    }

    protected $fillable = [
        'firstname', 'lastname', 'phone', 'email', 'password', 'anonimo', 'fotoperfil', 'api_token',
    ];

    protected $hidden = [
        'password',
    ];
}
