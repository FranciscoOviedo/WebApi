<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('/users/login',['uses' => 'UserController@getToken']);
$router->post('/users/create',['uses' => 'UserController@createUser']);
$router->get('/users/complaints/{id}',['uses' => 'UserController@userComplaints']);

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/complaints',['uses' => 'ComplaintController@getComplaints']);
$router->get('/complaints/detail/{id}',['uses' => 'ComplaintController@getComplaintDetails']);
$router->post('/complaints/create',['uses' => 'ComplaintController@createComplaint']);
$router->delete('/complaints/delete/{id}',['uses' => 'ComplaintController@deleteComplaint']);


$router->get('/bookmarks',['uses' => 'BookmarkController@getBookmarks']);
$router->post('/bookmarks/create',['uses' => 'BookmarkController@createBookmark']);
$router->delete('/bookmarks/delete/{id}',['uses' => 'BookmarkController@deleteBookmark']);

$router->get('/messages',['uses' => 'MessageController@getMessages']);
$router->post('/messages/create',['uses' => 'MessageController@createMessage']);
$router->delete('/messages/delete/{id}',['uses' => 'MessageController@deleteMessage']);

$router->get('/images',['uses' => 'ImageController@getImages']);
$router->post('/images/create',['uses' => 'ImageController@createImage']);
$router->delete('/images/delete/{id}',['uses' => 'ImageController@deleteImage']);

$router->get('/tagscomplaints',['uses' => 'TagComplaintController@getTagsComplaints']);
$router->post('/tagscomplaints/create',['uses' => 'TagComplaintController@createTagComplaint']);
$router->delete('/tagscomplaints/delete/{id}',['uses' => 'TagComplaintController@deleteTagComplaint']);

$router->get('/tags',['uses' => 'TagController@getTags']);
$router->post('/tags/create',['uses' => 'TagController@createTag']);
$router->delete('/tags/delete/{id}',['uses' => 'TagController@deleteTag']);


$router->group(['middleware' => ['auth']], function () use ($router){
    //Por ahora no pongo nada con necesidad de autorizacion
});

