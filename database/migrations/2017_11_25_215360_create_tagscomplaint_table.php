<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagscomplaintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagscomplaints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_complaint')->unsigned();
            $table->foreign('id_complaint')->references('id')->on('complaints');
            $table->integer('id_tag')->unsigned();
            $table->foreign('id_tag')->references('id')->on('tags');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagscomplaints');
    }
}
